// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "./Types.sol";

contract Multisig {
    struct SIGNATURE {
        bool processed;
        uint valid_till_block;
        uint[] sig_array;
    }

    uint constant internal valid_block_numbers = 100;
    uint constant internal min_owners_count = 1;
    uint constant internal max_owners_count = 7;
    address internal Owner;
    SON[] internal Owners;
    mapping(string => SIGNATURE) internal Signatures; //object_id = space_id.type_id.instance

    constructor(address _owner) {
        require(address(0) != _owner, "Zero address not allowed");
        Owner = _owner;
    }

    function ownerId(address _addr) private view returns (int){
        for (uint i; i< Owners.length; i++){
            if (Owners[i].addr ==_addr)
                return int(i);
        }

        return -1;
    }

    function ownerExist(address _addr) private view returns (bool){
        return ownerId(_addr) != -1;
    }

    modifier onlyOwners(address _sender) {
        require(((Owner == _sender) && (Owners.length == 0)) || ((Owner == address(0)) && (Owners.length > 0) && (ownerExist(_sender))), "Not an owner");
        _;
    }

    function proceedMultisig(address _sender, string memory _object_id) public onlyOwners(_sender) returns (bool) {
        int OwnerId;

        if(Owners.length > 0) {
            if (Signatures[_object_id].sig_array.length == 0) {
                Signatures[_object_id].processed = false;
                Signatures[_object_id].valid_till_block = block.number + valid_block_numbers;
                for (uint i; i< Owners.length; i++){
                    Signatures[_object_id].sig_array.push(0);
                }
            }

            OwnerId = ownerId(_sender);
        }
        else {
            if (Signatures[_object_id].sig_array.length == 0) {
                Signatures[_object_id].processed = false;
                Signatures[_object_id].valid_till_block = block.number + valid_block_numbers;
                Signatures[_object_id].sig_array.push(0);
            }
        }

        require(Signatures[_object_id].processed == false, "Transaction has already been processed");
        require(OwnerId > -1, "Wrong owner id");
        require(block.number <= Signatures[_object_id].valid_till_block, "Timeout for transaction signing");
        require(Signatures[_object_id].sig_array[uint(OwnerId)] == 0, "Owner has already sign transaction");
        Signatures[_object_id].sig_array[uint(OwnerId)] = 1;

        uint count = 0;
        for (uint i; i<Signatures[_object_id].sig_array.length; i++) {
            if (Signatures[_object_id].sig_array[i] == 1) {
                count += 1;
            }
        }

        if (uint(3) * count >= Signatures[_object_id].sig_array.length * uint(2)) {
            Signatures[_object_id].processed = true;
            // delete Signatures[_object_id]; -> We don't want to delete procceed transactions
            return true;
        }

        return false;
    }

    function updateOwners(address _sender, SON[] memory _newOwners, string memory _object_id) external {
        if(false == proceedMultisig(_sender, _object_id)){
            return;
        }

        require(((_newOwners.length >= min_owners_count) && (_newOwners.length <= max_owners_count)), "Invalid number of owners");

        Owner = address(0);
        delete Owners;
        for (uint i; i<_newOwners.length; i++) {
            Owners.push(_newOwners[i]);
        }
    }

    function getOwner() external view virtual returns (address) {
        return Owner;
    }

    function getOwners() external view virtual returns (SON[] memory) {
        return Owners;
    }
}