// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

struct SON {
    address addr;
    uint weight;
}