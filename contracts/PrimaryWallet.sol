// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./utils/Types.sol";

interface MultisigInterface {
    function proceedMultisig(address _sender, string memory _object_id) external returns (bool);
    function updateOwners(address _sender, SON[] memory _newOwners, string memory _object_id) external;
    function getOwner() external view returns (address);
    function getOwners() external view returns (SON[] memory);
}

interface ERC20 {
    function balanceOf(address owner) external view returns (uint);
    function allowance(address owner, address spender) external view returns (uint);
    function approve(address spender, uint value) external returns (bool);
    function transfer(address to, uint value) external returns (bool);
    function transferFrom(address from, address to, uint value) external returns (bool); 
}

contract PrimaryWallet is Initializable {
    struct ETH_SIGNATURE {
        uint8 v; 
        bytes32 r;
        bytes32 s;
    }

    struct TRANSACTION {
        bytes data;
        ETH_SIGNATURE signature;
    }

    event NewOwners(SON[] _previousOwners, SON[] _newOwners);
    event Deposit(address _from, uint _value);
    event DepositERC20(address _token, address _from, uint _value);
    event Withdraw(address _to, uint _value);
    event WithdrawERC20(address _token, address _to, uint _value);

    address internal MultisigAddress;
    address internal ProxyAdminAddress;

    function initialize(address _multisigAddress, address _proxyAdminAddress) external initializer {
        require(address(0) != _multisigAddress, "Zero address not allowed");

        MultisigAddress = _multisigAddress;
        ProxyAdminAddress = _proxyAdminAddress;
    }

    function updateOwners(TRANSACTION[] calldata transactions) external virtual {
        require(transactions.length != 0, "No transactions to process");

        for (uint i; i<transactions.length; i++) {
            require(bytes4(transactions[i].data) == bytes4(keccak256("updateOwners((address,uint256)[],string)")), "Wrong function signature");

            (SON[] memory newOwners, string memory object_id) = abi.decode(transactions[i].data[4:], (SON[], string));
            address from = ecrecover(keccak256(transactions[i].data), transactions[i].signature.v, transactions[i].signature.r, transactions[i].signature.s);

            updateOwners_(from, newOwners, object_id);
        }
    }
    
    function updateOwners(SON[] memory _newOwners, string memory _object_id) external virtual {
        updateOwners_(msg.sender, _newOwners, _object_id);
    }

    function updateOwners_(address _from, SON[] memory _newOwners, string memory _object_id) internal virtual {
        SON[] memory previousOwners = MultisigInterface(MultisigAddress).getOwners();

        MultisigInterface(MultisigAddress).updateOwners(_from, _newOwners, _object_id);

        emit NewOwners(previousOwners, _newOwners);
    }

    receive() external payable virtual {
        deposit();
    }

    function deposit() public payable virtual {
        emit Deposit(msg.sender, msg.value);
    }

    function depositERC20(address _token, uint _amount) external virtual {
        require(ERC20(_token).transferFrom(msg.sender, address(this), _amount) == true, "Error in transfer");
        emit DepositERC20(_token, msg.sender, _amount);
    }

    function withdraw(TRANSACTION[] calldata transactions) external payable virtual {
        require(transactions.length != 0, "No transactions to process");

        for (uint i; i<transactions.length; i++) {
            require(bytes4(transactions[i].data) == bytes4(keccak256("withdraw(address,uint256,string)")), "Wrong function signature");

            (address to, uint amount, string memory object_id) = abi.decode(transactions[i].data[4:], (address, uint, string));
            address from = ecrecover(keccak256(transactions[i].data), transactions[i].signature.v, transactions[i].signature.r, transactions[i].signature.s);

            withdraw_(from, payable(to), amount, object_id);
        }
    }

    function withdraw(address payable _to, uint _amount, string memory _object_id) external payable virtual {
        withdraw_(msg.sender, _to, _amount, _object_id);
    }

    function withdraw_(address _from, address payable _to, uint _amount, string memory _object_id) internal virtual {
        if(false == MultisigInterface(MultisigAddress).proceedMultisig(_from, _object_id)){
            return;
        }

        _to.transfer(_amount);

        emit Withdraw(_to, _amount);
    }

    function withdrawERC20(TRANSACTION[] calldata transactions) external virtual {
        require(transactions.length != 0, "No transactions to process");

        for (uint i; i<transactions.length; i++) {
            require(bytes4(transactions[i].data) == bytes4(keccak256("withdrawERC20(address,address,uint256,string)")), "Wrong function signature");

            (address token, address to, uint amount, string memory object_id) = abi.decode(transactions[i].data[4:], (address, address, uint, string));
            address from = ecrecover(keccak256(transactions[i].data), transactions[i].signature.v, transactions[i].signature.r, transactions[i].signature.s);

            withdrawERC20_(token, from, to, amount, object_id);
        }
    }

    function withdrawERC20(address _token, address _to, uint _amount, string memory _object_id) external virtual {
        withdrawERC20_(_token, msg.sender, _to, _amount, _object_id);
    }

    function withdrawERC20_(address _token, address _from, address _to, uint _amount, string memory _object_id) internal virtual {
        if(false == MultisigInterface(MultisigAddress).proceedMultisig(_from, _object_id)){
            return;
        }

        require(ERC20(_token).transfer(_to, _amount) == true, "Error in transfer");

        emit WithdrawERC20(_token, _to, _amount);
    }


    function getProxyAdmin() external view virtual returns (address) {
        return ProxyAdminAddress;
    }

    function getBalance() external view virtual returns (uint) {
        return address(this).balance;
    }

    function getERC20Balance(address _token) external view virtual returns (uint) {
        return ERC20(_token).balanceOf(address(this));
    }

    function getOwner() external view virtual returns (address) {
        return MultisigInterface(MultisigAddress).getOwner();
    }

    function getOwners() external view virtual returns (SON[] memory) {
        return MultisigInterface(MultisigAddress).getOwners();
    }

    function getBlockNumber() external view virtual returns (uint) {
        return block.number;
    }

    function getVersion() external pure virtual returns (string memory) {
        return "v1";
    }
}
