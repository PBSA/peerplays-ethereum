// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "./utils/Types.sol";

interface Proxy {
    function upgradeTo(address newImplementation) external;
}

interface MultisigInterface {
    function proceedMultisig(address _sender, string memory _object_id) external returns (bool);
}

contract ProxyAdminMultisig {
    address internal MultisigAddress;

    constructor(address _multisigAddress) {
        require(address(0) != _multisigAddress, "Zero address not allowed");
        MultisigAddress = _multisigAddress;
    }

    function upgrade(address _proxyAddress, address _implementationAddress, string memory _version) external {
        if(false == MultisigInterface(MultisigAddress).proceedMultisig(msg.sender, _version)){
            return;
        }

        Proxy proxy = Proxy(_proxyAddress);
        proxy.upgradeTo(_implementationAddress);
    }
}