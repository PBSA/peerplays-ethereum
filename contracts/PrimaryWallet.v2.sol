// SPDX-License-Identifier: MIT

pragma solidity ^0.8.9;

import "./PrimaryWallet.sol";

contract PrimaryWalletV2 is PrimaryWallet {
    function getVersion() external pure virtual override returns (string memory) {
        return "v2";
    }

    function test() external pure virtual returns (uint) {
        return 1;
    }
}
