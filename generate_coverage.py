from lxml import etree

THRESHOLD = float(90)

# Read html file
file_html = open("coverage/index.html", "r")
html = file_html.read()
tree = etree.HTML(html)
r = tree.xpath('/html/body/div[1]/div[3]/table/tbody/tr/td[9]')
result_coverage = r[0].text
result_coverage = float(result_coverage.replace('%', ''))
coverage_failure = bool(result_coverage <= THRESHOLD)
failures_number = 0
if coverage_failure :
    failures_number = 1
    
    
testsuites = etree.Element('testsuites', id='1', name='peerplays-ethereum', tests='1', failures=str(failures_number))
testsuite = etree.Element('testsuite', id='1', name='Smart contract test coverage', tests='1', failures=str(failures_number))
testsuites.append(testsuite)
testcase = etree.Element('testcase', id='1', name='hardhat coverage')
testsuite.append(testcase)
if coverage_failure :
    failure = etree.Element('failure', message='Coverage is below threshold: ' + str(result_coverage), type='ERROR')
    testcase.append(failure)

xml_str = etree.tostring(testsuites, pretty_print=True).decode("utf-8")
with open('coverage.xml', "w") as f:
    f.write(xml_str)

if coverage_failure :    
    with open('coverage.fail', "w") as f:
        f.write("fail") 