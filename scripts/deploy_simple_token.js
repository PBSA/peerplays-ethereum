const {ethers} = require("hardhat");

async function main() {
    const [deployer] = await ethers.getSigners();

    console.log("######################################################################################");
    console.log("Deploying SimpleToken");

    console.log("Deploying contracts with the account:", deployer.address);
    console.log("Account balance:", (await deployer.getBalance()).toString());

    const SimpleToken = await ethers.getContractFactory("SimpleToken");
    const SimpleTokenContract = await SimpleToken.deploy("Test ERC-20", "ERC20-TEST", 10000000);
    console.log("SimpleTokenContract address:", SimpleTokenContract.address);

    console.log("Name: ", await SimpleTokenContract.name());
    console.log("Symbol: ", await SimpleTokenContract.symbol());
    console.log("TotalSupply: ", await SimpleTokenContract.totalSupply());

    //! Send funds to accounts
    const accounts = [
        "0x5c79a9f5767e3c1b926f963fa24e21d8a04289ae",
        "0xef69a91be0dbbf044b416fcb819b2c23fd6aba22",
        "0xd0adc64f42d4241b2c1cf4d390e48142a084fc93",
        "0x6a0f279ddee8648a863d7251584e86104a4a51b7",
        "0xa2277cc7c8ae24f4f86e0d41bde9679e6ebb730b",
        "0x31ed87a390fb66e4c3f2c2cf8a7c59c1d2d7ddf4",
        "0x0e4830f56bb3aa3da70e9dbaaa472ebb6bd4549d",
        "0x3f6c7da283bef6d28b8d05948e6d81393f29e64f",
        "0x10d7b6dc29e6b16faea717dec18947d1fd70fba9",
        "0x338d889ee68955be449dfd0bb9379a6692a264a1",
        "0x0e89a2ea2b8582d12ca55899e153017354bc5227",
        "0x33226cdd64e9fc64d661e0ff878b8b93793795ff",
        "0x3500cab92ac344dfef85d51b5c03745ec9e3912b",
        "0xda3a94ae88dfd316f87184533664e66baea11a96",
        "0xc8dffcc547522e29af991e3a8235ce5a42c8116e",
        "0x9e706c947b0d7640fc1df14ff6373a9a92b73896"
    ];

    console.log("Begin of tokens transfer to accounts");
    for (const account of accounts){
        const transferTx = await SimpleTokenContract.transfer(account, 100);
        await transferTx.wait();
    }
    console.log("Tokens was transferred to accounts successfully");
}

main()
        .then(() => process.exit(0))
        .catch((error) => {
            console.error(error);
            process.exit(1);
        });
