const {ethers, upgrades} = require("hardhat");

async function main() {
    const [deployer, new_owner2, new_owner3, new_owner4, new_owner5] = await ethers.getSigners();

    console.log("######################################################################################");
    console.log("Deploying PrimaryWallet");

    console.log("Deploying contracts with the account:", deployer.address);
    console.log("Account balance:", (await deployer.getBalance()).toString());

    const Multisig = await ethers.getContractFactory("Multisig");
    MultisigContract = await Multisig.deploy(deployer.address);
    console.log("MultisigContract address:", MultisigContract.address);

    const ProxyAdminMultisig = await ethers.getContractFactory("ProxyAdminMultisig");
    ProxyAdminMultisigContract = await ProxyAdminMultisig.deploy(MultisigContract.address);
    console.log("ProxyAdminMultisigContract address:", ProxyAdminMultisigContract.address);

    const PrimaryWallet = await ethers.getContractFactory("PrimaryWallet");
    const PrimaryWalletContract = await upgrades.deployProxy(PrimaryWallet, [MultisigContract.address, ProxyAdminMultisigContract.address]);
    await PrimaryWalletContract.deployed();
    console.log("PrimaryWalletContract address:", PrimaryWalletContract.address);

    const ProxyAdminAddress = await PrimaryWalletContract.getProxyAdmin();
    await upgrades.admin.changeProxyAdmin(PrimaryWalletContract.address, ProxyAdminAddress);
    console.log("Changing ProxyAdmin successful");

    const new_owners = [
        {addr: deployer.address, weight: 1},
        {addr: new_owner2.address, weight: 1},
        {addr: new_owner3.address, weight: 1},
        {addr: new_owner4.address, weight: 1},
        {addr: new_owner5.address, weight: 1}
    ];
    const object_id = "init_owners";
    const abi = JSON.parse('[ 	{ "inputs": [ { "components": [ { "internalType": "address", "name": "addr", "type": "address" }, { "internalType": "uint256", "name": "weight", "type": "uint256" } ], "internalType": "struct PrimaryWallet.SON[]", "name": "_newOwners", "type": "tuple[]" }, { "internalType": "string", "name": "_object_id", "type": "string" } ], "name": "updateOwners", "outputs": [], "stateMutability": "nonpayable", "type": "function" } ]');
    const ContractInstance = new ethers.Contract(PrimaryWalletContract.address, abi, deployer);
    await ContractInstance.updateOwners(new_owners, object_id);
    console.log("updateOwners successful");
}

main()
        .then(() => process.exit(0))
        .catch((error) => {
            console.error(error);
            process.exit(1);
        });
