const {ethers} = require("hardhat");

async function main() {
    const [,,,,,,,,,,,,,,,,Owner] = await ethers.getSigners();

    const PrimaryWalletContractAddress = "0x56316f7354845275F28478d53F8dd423E42cfCe6";
    const SimpleTokenContractAddress = "0xC11Ae091d112D6e2E7a45065E307B29e5DF74706";

    const abi_primary_wallet = JSON.parse('[ { "inputs": [ { "internalType": "address", "name": "_token", "type": "address" }, { "internalType": "uint256", "name": "_amount", "type": "uint256" } ], "name": "depositERC20", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "_token", "type": "address" } ], "name": "getERC20Balance", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" } ]');
    const PrimaryWalletContractInstance = new ethers.Contract(PrimaryWalletContractAddress, abi_primary_wallet, Owner);

    const abi_simple_token = JSON.parse('[ { "inputs": [ { "internalType": "address", "name": "owner", "type": "address" }, { "internalType": "address", "name": "spender", "type": "address" } ], "name": "allowance", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "value", "type": "uint256" } ], "name": "approve", "outputs": [ { "internalType": "bool", "name": "", "type": "bool" } ], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "owner", "type": "address" } ], "name": "balanceOf", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" } ]');
    const SimpleTokenContractInstance = new ethers.Contract(SimpleTokenContractAddress, abi_simple_token, Owner);

    console.log("Owner:", Owner.address);
    console.log("Balance of owner: ", await SimpleTokenContractInstance.balanceOf(Owner.address));
    console.log("Balance before deposit: ", await PrimaryWalletContractInstance.getERC20Balance(SimpleTokenContractAddress));
    const approveTx = await SimpleTokenContractInstance.approve(PrimaryWalletContractAddress, 10);
    await approveTx.wait();
    //console.log("Allowance: ", await SimpleTokenContractInstance.allowance(Owner.address, PrimaryWalletContractAddress));
    const depositERC20Tx = await PrimaryWalletContractInstance.depositERC20(SimpleTokenContractAddress, 10);
    await depositERC20Tx.wait();
    console.log("Balance after deposit: ", await PrimaryWalletContractInstance.getERC20Balance(SimpleTokenContractAddress));
}

main()
        .then(() => process.exit(0))
        .catch((error) => {
            console.error(error);
            process.exit(1);
        });
